<?php
// +----------------------------------------------------------------------
// | SentCMS [ WE CAN DO IT JUST THINK IT ]
// +----------------------------------------------------------------------
// | Copyright (c) 2013 http://www.tensent.cn All rights reserved.
// +----------------------------------------------------------------------
// | Author: molong <molong@tensent.cn> <http://www.tensent.cn>
// +----------------------------------------------------------------------
namespace tensent\yspay\common;

use tensent\yspay\Kernel\BaseClient;

class Client extends BaseClient{

	/**
	 * @title 文件上传（未完善）
	 *
	 * @param [type] $params
	 * @return void
	 */
	public function uploadDocument($params){
		$myParams = ['serviceNo'  => 'uploadDocument'];

		$data = [
			'requestNo' => isset($params['requestNo']) ? $params['requestNo'] : '',
			'fileType'   => isset($params['fileType']) ? $params['fileType'] : '',    //文件类型必须与实际上传文件类型一致，否则会导致上传文件无效jpg、png、pdf、excel、mp4、avi等
			'fileUrl'   => isset($params['fileUrl']) ? $params['fileUrl'] : '',   //远程文件链接，链接不可跳转、重定向、身份验证fileUrl、fileBase64必传一项，两项都传只取fileBase64
			'fileBase64' => isset($params['fileBase64']) ? $params['fileBase64'] : '',  //文件Base64编码，文件大小参考具体业务接口的要求，常规情况不超过2M, fileUrl、fileBase64必传一项，两项都传只取fileBase64
			'fileName'   => isset($params['fileName']) ? $params['fileName'] : ''  //文件名称
		];

		$this->check_require($data, ['requestNo', 'fileType']);

		$myParams['bizReqJson'] = \GuzzleHttp\json_encode($data, JSON_UNESCAPED_UNICODE);
		return $this->app->basic->httpUpload($this->api_urls['file'], $myParams, 'bizResponseJson');
	}


	/**
	 * @title 短信确认验证
	 *
	 * @param [type] $params
	 * @return void
	 */
	public function confirmVerify($params){
		$myParams = ['serviceNo'  => 'confirmVerify'];

		$data = [
			'requestNo' => isset($params['requestNo']) ? $params['requestNo'] : '',
			'authSn' => isset($params['authSn']) ? $params['authSn'] : '',
			'smsCode' => isset($params['smsCode']) ? $params['smsCode'] : ''
		];
		$this->check_require($data, [
			'requestNo',
			'authSn',
			'smsCode'
		]);
		$myParams['bizReqJson'] = \GuzzleHttp\json_encode($data, JSON_UNESCAPED_UNICODE);
		return $this->app->basic->httpPost($this->api_urls['merchant'], $myParams, 'bizResponseJson');
	}

	/**
	 * @title 重发验证码
	 *
	 * @param [type] $params
	 * @return void
	 */
	public function resendCode($params){
		$myParams = ['serviceNo'  => 'resendCode'];

		$data = [
			'requestNo' => isset($params['requestNo']) ? $params['requestNo'] : ''
		];
		$this->check_require($data, [
			'requestNo'
		]);
		$myParams['bizReqJson'] = \GuzzleHttp\json_encode($data, JSON_UNESCAPED_UNICODE);
		return $this->app->basic->httpPost($this->api_urls['merchant'], $myParams, 'bizResponseJson');
	}

	/**
	 * @title 修改手机号
	 *
	 * @param [type] $params
	 * @return void
	 */
	public function modifyMobile($params){
		$myParams = ['serviceNo'  => 'modifyMobile'];

		$data = [
			'requestNo' => isset($params['requestNo']) ? $params['requestNo'] : '',
			'merchantNo' => isset($params['merchantNo']) ? $params['merchantNo'] : '',
			'mobile' => isset($params['mobile']) ? $params['mobile'] : '',
			'newMobile' => isset($params['newMobile']) ? $params['newMobile'] : ''
		];

		$this->check_require($data, [
			'requestNo',
			'merchantNo',
			'mobile',
			'newMobile'
		]);

		$myParams['bizReqJson'] = \GuzzleHttp\json_encode($data, JSON_UNESCAPED_UNICODE);
		return $this->app->basic->httpPost($this->api_urls['merchant'], $myParams, 'bizResponseJson');
	}
}