<?php
// +----------------------------------------------------------------------
// | SentCMS [ WE CAN DO IT JUST THINK IT ]
// +----------------------------------------------------------------------
// | Copyright (c) 2013 http://www.tensent.cn All rights reserved.
// +----------------------------------------------------------------------
// | Author: molong <molong@tensent.cn> <http://www.tensent.cn>
// +----------------------------------------------------------------------
namespace tensent\yspay\common;

use Pimple\Container;
use Pimple\ServiceProviderInterface;

class CommonProvider implements ServiceProviderInterface{
    
    public function register(Container $pimple) {
        $pimple['common'] = function ($app){
            return new Client($app);
        };
    }
}