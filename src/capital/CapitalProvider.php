<?php
namespace tensent\yspay\capital;

use Pimple\Container;
use Pimple\ServiceProviderInterface;

class CapitalProvider implements ServiceProviderInterface{
    
    public function register(Container $pimple) {
        $pimple['capital'] = function ($app){
            return new Client($app);
        };
    }
}