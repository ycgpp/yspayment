<?php
// +----------------------------------------------------------------------
// | SentCMS [ WE CAN DO IT JUST THINK IT ]
// +----------------------------------------------------------------------
// | Copyright (c) 2013 http://www.tensent.cn All rights reserved.
// +----------------------------------------------------------------------
// | Author: molong <molong@tensent.cn> <http://www.tensent.cn>
// +----------------------------------------------------------------------
namespace tensent\yspay\capital;

use tensent\yspay\Kernel\BaseClient;

class Client extends BaseClient{

	/**
	 * @title 分账注册
	 *
	 * @param [type] $params
	 * @return void
	 */
	public function divisionRegister($params){
		$myParams = ['serviceNo'  => 'divisionRegister'];

		$data = [
			'requestNo' => isset($params['requestNo']) ? $params['requestNo'] : '',
			'payeeMerchantNo'   => isset($params['payeeMerchantNo']) ? $params['payeeMerchantNo'] : '',    //收款方银盛商户号
			'origRequestNo'   => isset($params['origRequestNo']) ? $params['origRequestNo'] : '',   //需分账订单的业务请求号
			'amount'   => isset($params['amount']) ? $params['amount'] : '',  //原订单金额，单位：元
			'isDivision'   => isset($params['isDivision']) ? $params['isDivision'] : '',  //原订单是否分账，Y-是, N-否（无需分账订单传N，直接入账到收款方）
			'divisionMode' => isset($params['divisionMode']) ? $params['divisionMode'] : '',  //分账模式  01-比例、02-金额
			'divisionList' => isset($params['divisionList']) ? $params['divisionList'] : [],  //分账明细列表，是否分账为Y时，此参数必传
			'notifyUrl' => isset($params['notifyUrl']) ? $params['notifyUrl'] : '',  //分账结果异步通知地址，为空则不通知
		];

		$this->check_require($data, ['requestNo', 'payeeMerchantNo', 'origRequestNo', 'amount', 'isDivision', 'divisionMode']);

		if($data['isDivision'] == 'Y' && count($data['divisionList']) == 0){
			throw new \InvalidArgumentException(sprintf('%s must require', 'divisionList'));
		}

		$myParams['bizReqJson'] = \GuzzleHttp\json_encode($data, JSON_UNESCAPED_UNICODE);
		return $this->app->basic->httpPost($this->api_urls['trade'], $myParams, 'bizResponseJson');
	}

	/**
	 * @title 分账查询
	 *
	 * @param [type] $params
	 * @return void
	 */
	public function divisionQuery($params){
		$myParams = ['serviceNo'  => 'divisionQuery'];

		$data = [
			'origRequestNo'   => isset($params['origRequestNo']) ? $params['origRequestNo'] : '',   //原交易订单业务请求号
		];

		$this->check_require($data, ['origRequestNo']);

		$myParams['bizReqJson'] = \GuzzleHttp\json_encode($data, JSON_UNESCAPED_UNICODE);
		return $this->app->basic->httpPost($this->api_urls['trade'], $myParams, 'bizResponseJson');
	}

	/**
	 * @title 账户余额查询
	 *
	 * @param [type] $params
	 * @return void
	 */
	public function queryBalance($params){
		$myParams = ['serviceNo'  => 'queryBalance'];

		$data = [
			'merchantNo'   => isset($params['merchantNo']) ? $params['merchantNo'] : '',   //银盛商户号
		];

		$this->check_require($data, ['merchantNo']);

		$myParams['bizReqJson'] = \GuzzleHttp\json_encode($data, JSON_UNESCAPED_UNICODE);
		return $this->app->basic->httpPost($this->api_urls['merchant'], $myParams, 'bizResponseJson');
	}

	/**
	 * @title 平台内转账申请
	 *
	 * @param [type] $params
	 * @return void
	 */
	public function transferInner($params){
		$myParams = ['serviceNo'  => 'transferInner'];

		$data = [
			'requestNo'   => isset($params['requestNo']) ? $params['requestNo'] : '',   //转账业务请求号
			'payeeMerchantNo'   => isset($params['payeeMerchantNo']) ? $params['payeeMerchantNo'] : '',
			'payerMerchantNo'   => isset($params['payerMerchantNo']) ? $params['payerMerchantNo'] : '',   //银盛商户号
			'orderDesc'   => isset($params['orderDesc']) ? $params['orderDesc'] : '',   //转账描述
			'amount'   => isset($params['amount']) ? $params['amount'] : '',   //转账金额
			'transferType'   => isset($params['transferType']) ? $params['transferType'] : '',   //转账类型  P2B-平台转商户 P2C-平台转用户 B2P-商户转平台 B2B-商户转商户  B2C-商户转用户 C2C-用户转用户 A2A-资金归集
			'redirectUrl'   => isset($params['redirectUrl']) ? $params['redirectUrl'] : '',  //重定向链接，业务成功后重定向到此链接	当验证权限为密码校验时必填
			'notifyUrl'   => isset($params['notifyUrl']) ? $params['notifyUrl'] : '',   //转账结果异步通知地址，为空则不通知
		];

		$this->check_require($data, ['requestNo', 'payeeMerchantNo', 'payerMerchantNo', 'orderDesc', 'amount', 'transferType']);

		$myParams['bizReqJson'] = \GuzzleHttp\json_encode($data, JSON_UNESCAPED_UNICODE);
		return $this->app->basic->httpPost($this->api_urls['merchant'], $myParams, 'bizResponseJson');
	}

	/**
	 * @title 平台内转账查询
	 *
	 * @param [type] $params
	 * @return void
	 */
	public function transferInnerRefund($params){
		$myParams = ['serviceNo'  => 'transferInnerRefund'];

		$data = [
			'requestNo'   => isset($params['requestNo']) ? $params['requestNo'] : '',   //转账业务请求号
			'origRequestNo'   => isset($params['origRequestNo']) ? $params['origRequestNo'] : '',
			'payeeMerchantNo'   => isset($params['payeeMerchantNo']) ? $params['payeeMerchantNo'] : '',
			'payerMerchantNo'   => isset($params['payerMerchantNo']) ? $params['payerMerchantNo'] : '',   //银盛商户号
			'reason'   => isset($params['reason']) ? $params['reason'] : '',   //转账描述
		];

		$this->check_require($data, ['requestNo', 'origRequestNo', 'payeeMerchantNo', 'payerMerchantNo', 'reason']);

		$myParams['bizReqJson'] = \GuzzleHttp\json_encode($data, JSON_UNESCAPED_UNICODE);
		return $this->app->basic->httpPost($this->api_urls['merchant'], $myParams, 'bizResponseJson');
	}
}