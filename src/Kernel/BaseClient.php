<?php
// +----------------------------------------------------------------------
// | SentCMS [ WE CAN DO IT JUST THINK IT ]
// +----------------------------------------------------------------------
// | Copyright (c) 2013 http://www.tensent.cn All rights reserved.
// +----------------------------------------------------------------------
// | Author: molong <molong@tensent.cn> <http://www.tensent.cn>
// +----------------------------------------------------------------------
namespace tensent\yspay\Kernel;

use tensent\yspay\Client;

class BaseClient{
	
	protected $app;
	protected $charset = 'UTF-8';
	protected $version = 'v2.0.0';

	protected $api_urls = [];

	public function __construct(Client $app) {
		$this->app = $app;
		$this->api_urls = $this->getUrl();
	}


	public function check_require($data, $need_params) {
		foreach ($need_params as $val){
			if(!isset($data[$val]) || empty($data[$val])){
				throw new \InvalidArgumentException(sprintf('%s must require', $val));
			}
		}
	}

	public function publicParams() {
		return [
			'requestId'         => $this->uuid(),
			'srcMerchantNo'       => $this->app->config->partner_id,
			'version'           => $this->version,
			'charset'           => $this->charset,
			'signType'         => $this->app->config->sign_type
		];
	}

	public function uuid(){
		$chars = md5(uniqid(mt_rand(), true));
		$uuid = substr($chars, 0, 8) . '-' .
			substr($chars, 8, 4) . '-' .
			substr($chars, 12, 4) . '-' .
			substr($chars, 16, 4) . '-' .
			substr($chars, 20, 12);
		
		return $uuid;
	}

	public function getUrl(){
		$production = [
			'trade'  => 'https://eqt.ysepay.com/api/trade',
			'merchant' => 'https://eqt.ysepay.com/api/merchant',
			'file' => 'https://eqt.ysepay.com/api/file'
		];

		$urls = $production;

		return $urls;
	}
}