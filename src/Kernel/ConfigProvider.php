<?php
// +----------------------------------------------------------------------
// | SentCMS [ WE CAN DO IT JUST THINK IT ]
// +----------------------------------------------------------------------
// | Copyright (c) 2013 http://www.tensent.cn All rights reserved.
// +----------------------------------------------------------------------
// | Author: molong <molong@tensent.cn> <http://www.tensent.cn>
// +----------------------------------------------------------------------
namespace tensent\yspay\Kernel;

use Pimple\Container;
use Pimple\ServiceProviderInterface;
use tensent\yspay\Client;

class ConfigProvider implements ServiceProviderInterface {

	public function register(Container $app){
		$app['config'] = function (Client $app) {
			return new Config($app->getConfig());
		};
	}
}