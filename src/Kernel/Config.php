<?php
// +----------------------------------------------------------------------
// | SentCMS [ WE CAN DO IT JUST THINK IT ]
// +----------------------------------------------------------------------
// | Copyright (c) 2013 http://www.tensent.cn All rights reserved.
// +----------------------------------------------------------------------
// | Author: molong <molong@tensent.cn> <http://www.tensent.cn>
// +----------------------------------------------------------------------
namespace tensent\yspay\Kernel;

use tensent\yspay\Client;

class Config{

	public $business_code;
	public $partner_id;
	public $seller_id;
	public $seller_name;
	public $notify_url;
	public $return_url;
	public $private_cert;
	public $pfxpassword;
	/**
	 * //公钥路径
	 *
	 * @var String
	 */
	public $businessgatecerpath;
	public $response_type;
	public $sandbox;
	public $appid;
	public $merchant_code;

	public $log;
	public $sign_type;
	public $weapp_appid;

	public function __construct(array $config = []){
		$this->business_code=$config['business_code'];
		$this->businessgatecerpath=$config['business_gate_cert'];
		$this->private_cert=$config['private_cert'];
		$this->partner_id=$config['partner_id'];
		$this->seller_id=$config['seller_id'];
		$this->seller_name=$config['seller_name'];
		$this->pfxpassword=$config['pfxpassword'];
		$this->merchant_code=$config['merchant_code'];

		$this->log=$config['log'];

		$this->sign_type=$config['sign_type'];
		$this->sandbox=$config['sandbox'];

		$this->appid=$config['appid'];
		$this->weapp_appid=$config['weapp_appid'];
	}
}