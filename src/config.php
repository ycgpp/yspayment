<?php
return [
	'business_gate_cert'    => '',    //公钥路径
	'private_cert'          => './private_key.pem',           //私钥路径
	'partner_id'            => '8264********061', //商户号
	'seller_id'             => '',
	'seller_name'           => '',

	'pfxpassword'           => '',
	'merchant_code'         => '',

	'business_code'         => '',
	'log'   => [
		'path' => __DIR__ . '/paylog/'.time().'.log',
		'name'  => 'ysepay',
	],
	'sign_type'       => 'RSA',
	'appid'            => '公众号appid',
	'weapp_appid' => '小程序appid',
];