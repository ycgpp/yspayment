<?php
// +----------------------------------------------------------------------
// | SentCMS [ WE CAN DO IT JUST THINK IT ]
// +----------------------------------------------------------------------
// | Copyright (c) 2013 http://www.tensent.cn All rights reserved.
// +----------------------------------------------------------------------
// | Author: molong <molong@tensent.cn> <http://www.tensent.cn>
// +----------------------------------------------------------------------
namespace tensent\yspay\trade;

use tensent\yspay\Kernel\BaseClient;

class Client extends BaseClient{

	/**
	 * 聚合JS支付
	 * @author molong <molong@tensent.cn>
	 * @date 2019-08-06 9:59
	 */
	public function jsPay($params){
		$myParams = ['serviceNo'  => 'jsPay'];

		$data = [
			'payeeMerchantNo' => isset($params['payeeMerchantNo']) ? $params['payeeMerchantNo'] : '', // 收款方银盛商户号
			'requestNo' => isset($params['order_number']) ? $params['order_number'] : '', // 商户系统生成的订单号
			'amount' => isset($params['amount']) ? $params['amount'] : '', // 交易金额 number_format($price, 2, '.', '')
			'orderDesc' => isset($params['orderDesc']) ? $params['orderDesc'] : '', // 商品名称/订单标题 
			'bankType' => isset($params['bankType']) ? $params['bankType'] : '', // 支付渠道 支付渠道1902000-微信(支持payMode：28、29) 1903000-支付宝(支持payMode：26)9001002-银联(支持payMode：30)
			'payMode' => isset($params['payMode']) ? $params['payMode'] : '', //支付方式26-支付宝生活号 、28-微信公众号 29-微信小程序、30-银联行业码支付
			'wxAppId' => isset($params['wxAppId']) ? $params['wxAppId'] : '',
			'wxOpenId' => isset($params['wxOpenId']) ? $params['wxOpenId'] : '',
			'alipayId'  => isset($params['alipayId']) ? $params['alipayId'] : '',
			'unionUserId' => isset($params['unionUserId']) ? $params['unionUserId'] : '',
			'notifyUrl' => isset($params['notifyUrl']) ? $params['notifyUrl'] : '', // 支付成功结果异步通知地址
		];
		$this->check_require($data, ['payeeMerchantNo', 'requestNo', 'amount', 'orderDesc', 'bankType', 'payMode', 'notifyUrl']);

		$myParams['bizReqJson'] = \GuzzleHttp\json_encode($data, JSON_UNESCAPED_UNICODE);//构造字符串
		return $this->app->basic->httpPost( $this->api_urls['trade'], $myParams, 'bizResponseJson');
	}

	/**
	 * 订单查询
	 * @author molong <molong@tensent.cn>
	 * @date 2019-08-06 9:59
	 */
	public function queryOrder($params){
		$myParams = ['serviceNo'  => 'queryUnifyOrder'];

		$data = ['requestNo' => isset($params['order_number']) ? $params['order_number'] : ''];

		$this->check_require($data, ['requestNo']);

		$myParams['bizReqJson'] = \GuzzleHttp\json_encode($data, JSON_UNESCAPED_UNICODE);
		return $this->app->basic->httpPost( $this->api_urls['trade'], $myParams, 'bizResponseJson');
	}

	/**
	 * @title 退款
	 *
	 * @param [type] $params
	 * @return void
	 */
	public function refund($params){
		$myParams = ['serviceNo'  => 'refund'];

		$data = [
			'requestNo' => isset($params['order_number']) ? $params['order_number'] : '', // 商户系统生成的订单号
			'origRequestNo' => isset($params['origRequestNo']) ? $params['origRequestNo'] : '',  //原交易订单业务请求号
			'origTradeSn'  => isset($params['origTradeSn']) ? $params['origTradeSn'] : '', //原交易订单交易流水号
			'amount' => isset($params['amount']) ? $params['amount'] : '', // 退款金额
			'reason' => isset($params['reason']) ? $params['reason'] : '', // 退款原因
			'isDivision'   => isset($params['isDivision']) ? $params['isDivision'] : 'N',  //原交易订单是否分账	Y-是、N-否
		];
		if(isset($params['refundSource'])){
			$data['refundSource'] = $params['refundSource'];
		}
		if(isset($params['tranType'])){
			$data['tranType'] = $params['tranType'];
		}
		if(isset($params['origAmount'])){
			$data['origAmount'] = $params['origAmount'];
		}
		if(isset($params['refundSplitInfo'])){
			$data['refundSplitInfo'] = $params['refundSplitInfo'];
		}
		if(isset($params['notifyUrl'])){
			$data['notifyUrl'] = $params['notifyUrl'];
		}
		$this->check_require($data, ['requestNo', 'amount', 'reason', 'notifyUrl']);

		$myParams['bizReqJson'] = \GuzzleHttp\json_encode($data, JSON_UNESCAPED_UNICODE);
		return $this->app->basic->httpPost($this->api_urls['trade'], $myParams, 'bizResponseJson');
	}

	/**
	 * @title 退款查询
	 *
	 * @param [type] $params
	 * @return void
	 */
	public function queryRefund($params){
		$myParams = ['serviceNo'  => 'queryTradeRefund'];

		$data = ['requestNo' => isset($params['order_number']) ? $params['order_number'] : ''];

		$this->check_require($data, ['requestNo']);

		$myParams['bizReqJson'] = \GuzzleHttp\json_encode($data, JSON_UNESCAPED_UNICODE);
		return $this->app->basic->httpPost($this->api_urls['trade'], $myParams, 'bizResponseJson');
	}

	/**
	 * @title 关闭订单
	 *
	 * @param [type] $params
	 * @return void
	 */
	public function closeOrder($params){
		$myParams = ['serviceNo'  => 'closeOrder'];

		$data = [
			'requestNo' => isset($params['order_number']) ? $params['order_number'] : '', // 商户系统生成的订单号
			'payeeMerchantNo' => isset($params['payeeMerchantNo']) ? $params['payeeMerchantNo'] : '', // 收款方银盛商户号
		];

		$this->check_require($data, ['requestNo', 'payeeMerchantNo']);
		$myParams['bizReqJson'] = \GuzzleHttp\json_encode($data, JSON_UNESCAPED_UNICODE);
		return $this->app->basic->httpPost($this->api_urls['trade'], $myParams, 'bizResponseJson');
	}
}