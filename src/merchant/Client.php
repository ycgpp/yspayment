<?php
// +----------------------------------------------------------------------
// | SentCMS [ WE CAN DO IT JUST THINK IT ]
// +----------------------------------------------------------------------
// | Copyright (c) 2013 http://www.tensent.cn All rights reserved.
// +----------------------------------------------------------------------
// | Author: molong <molong@tensent.cn> <http://www.tensent.cn>
// +----------------------------------------------------------------------
namespace tensent\yspay\merchant;

use tensent\yspay\Kernel\BaseClient;

class Client extends BaseClient{

	/**
	 * @title 商户进件
	 *
	 * @param [type] $params
	 * @return void
	 */
	public function register($params){
		$myParams = ['serviceNo'  => 'register'];

		$data = [
			'outMerchantNo'  => isset($params['outMerchantNo']) ? $params['outMerchantNo'] : '',   //外部商户号，由平台商提供，每次请求保证唯一
			'merchantType' => isset($params['merchantType']) ? $params['merchantType'] : '', // 商户类型
			'merchantName' => isset($params['merchantName']) ? $params['merchantName'] : '', // 商户名称
			'merchantShortName' => isset($params['merchantShortName']) ? $params['merchantShortName'] : '', // 商户简称 ，用于显示收款方名称
			'businessCode'  => isset($params['businessCode']) ? $params['businessCode'] : '', // 营业执照号 商户类型为个体、企业时必填
			'businessName' => isset($params['businessName']) ? $params['businessName'] : '', // 营业执照全称 商户类型为个体、企业时必填
			'businessExpire' => isset($params['businessExpire']) ? $params['businessExpire'] : '', // 营业执照起始日期，格式：yyyyMMdd
			'businessFailure' => isset($params['businessFailure']) ? $params['businessFailure'] : '',  //营业执照失效日期，格式：yyyyMMdd长期填29991231
			'businessAddress' => isset($params['businessAddress']) ? $params['businessAddress'] : '',  //营业执照注册地址
			'bizAddress' => isset($params['bizAddress']) ? $params['bizAddress'] : '',  //经营地址,注：地址需要与所在市一致否则会进件失败
			'province'  => isset($params['province']) ? $params['province'] : '',
			'city'  => isset($params['city']) ? $params['city'] : '',
			'region' => isset($params['region']) ? $params['region'] : '',
			'lawyerName' => isset($params['lawyerName']) ? $params['lawyerName'] : '',
			'lawyerPhone' => isset($params['lawyerPhone']) ? $params['lawyerPhone'] : '',
			'lawyerId' =>  isset($params['lawyerId']) ? $params['lawyerId'] : '',
			'lawyerIdExpired' => isset($params['lawyerIdExpired']) ? $params['lawyerIdExpired'] : '',
			'lawyerIdFailure' => isset($params['lawyerIdFailure']) ? $params['lawyerIdFailure'] : '',
			'lawyerCountry' => isset($params['lawyerCountry']) ? $params['lawyerCountry'] : '',
			'lawyerWork' => isset($params['lawyerWork']) ? $params['lawyerWork'] : '',  //C	法人职业（传编号）
			'lawyerSex' => isset($params['lawyerSex']) ? $params['lawyerSex'] : '',  //法人性别：01-男 02-女 商户类型为个体、企业时必填
			'lawyerAddress' => isset($params['lawyerAddress']) ? $params['lawyerAddress'] : '',
			'lawyerIdType' => isset($params['lawyerIdType']) ? $params['lawyerIdType'] : '',
			'accountCardType' => isset($params['accountCardType']) ? $params['accountCardType'] : '',
			'accountCardno' => isset($params['accountCardno']) ? $params['accountCardno'] : '',
			'accountCardnoName' => isset($params['accountCardnoName']) ? $params['accountCardnoName'] : '',
			'openAccProvince' => isset($params['openAccProvince']) ? $params['openAccProvince'] : '',
			'openAccCity' => isset($params['openAccCity']) ? $params['openAccCity'] : '',
			'openAccArea' => isset($params['openAccArea']) ? $params['openAccArea'] : '',
			'openAccBanktype' => isset($params['openAccBanktype']) ? $params['openAccBanktype'] : '',
			'personName' => isset($params['personName']) ? $params['personName'] : '',
			'personId'  => isset($params['personId']) ? $params['personId'] : '',
			'personPhone' => isset($params['personPhone']) ? $params['personPhone'] : '',
			'personEmail' => isset($params['personEmail']) ? $params['personEmail'] : '',
			'personAddress' => isset($params['personAddress']) ? $params['personAddress'] : '',
			'mccCode' => isset($params['mccCode']) ? $params['mccCode'] : '',
			'lowestStlAmt' => isset($params['lowestStlAmt']) ? $params['lowestStlAmt'] : '',
			'settleType' => isset($params['settleType']) ? $params['settleType'] : '',
			'reportType' => isset($params['reportType']) ? $params['reportType'] : '',
			'notifyType' => isset($params['notifyType']) ? $params['notifyType'] : '',
			'notifyAddress' => isset($params['notify_url']) ? $params['notify_url'] : '',
			'businessPic' => isset($params['businessPic']) ? $params['businessPic'] : '',
			'accountCardPicFront' => isset($params['accountCardPicFront']) ? $params['accountCardPicFront'] : '',
			'lawyerIdPicFront' => isset($params['lawyerIdPicFront']) ? $params['lawyerIdPicFront'] : '',
			'lawyerIdPicBack' => isset($params['lawyerIdPicBack']) ? $params['lawyerIdPicBack'] : '',
			'lawyerIdHandFront' => isset($params['lawyerIdHandFront']) ? $params['lawyerIdHandFront'] : '',
			'doorFrontPic' => isset($params['doorFrontPic']) ? $params['doorFrontPic'] : '',
			'cashierPic' => isset($params['cashierPic']) ? $params['cashierPic'] : '',
			'scenePic' => isset($params['scenePic']) ? $params['scenePic'] : '',
		];

		$this->check_require($data, [
			'outMerchantNo',
			'merchantType',
			'merchantName',
			'merchantShortName',
			'bizAddress',
			'province',
			'city',
			'lawyerName',
			'lawyerPhone',
			'lawyerId',
			'lawyerIdExpired',
			'lawyerIdFailure',
			'lawyerIdType',
			'accountCardType',
			'accountCardno',
			'accountCardnoName',
			'openAccProvince',
			'openAccCity',
			'openAccBanktype',
			'personName',
			'personId',
			'personPhone',
			'personEmail',
			'personAddress',
			'mccCode',
			'accountCardPicFront',
			'lawyerIdPicFront',
			'lawyerIdPicBack',
			'lawyerIdHandFront',
			'doorFrontPic',
			'cashierPic',
			'scenePic'
		]);


		$myParams['bizReqJson'] = \GuzzleHttp\json_encode($data, JSON_UNESCAPED_UNICODE);
		return $this->app->basic->httpPost($this->api_urls['merchant'], $myParams, 'bizResponseJson');
	}

	/**
	 * @title 商户信息查询
	 *
	 * @param [type] $params
	 * @return void
	 */
	public function query($params){
		$myParams = ['serviceNo'  => 'query'];

		$data = [
			'merchantNo' => isset($params['merchantNo']) ? $params['merchantNo'] : '',
			'outMerchantNo' => isset($params['outMerchantNo']) ? $params['outMerchantNo'] : '',
		];

		$myParams['bizReqJson'] = \GuzzleHttp\json_encode($data, JSON_UNESCAPED_UNICODE);
		return $this->app->basic->httpPost($this->api_urls['merchant'], $myParams, 'bizResponseJson');
	}

	/**
	 * @title 商户绑定提现卡
	 *
	 * @param [type] $params
	 * @return void
	 */
	public function addBankAccount($params){
		$myParams = ['serviceNo'  => 'addBankAccount'];

		$data = [
			'requestNo'  => isset($params['requestNo']) ? $params['requestNo'] : '',
			'merchantNo' => isset($params['merchantNo']) ? $params['merchantNo'] : '',
			'accountCardType' => isset($params['accountCardType']) ? $params['accountCardType'] : '',
			'accountNo' => isset($params['accountNo'])? $params['accountNo'] : '',
			'accountName' => isset($params['accountName']) ? $params['accountName'] : '',
			'openAccBanktype' => isset($params['openAccBanktype']) ? $params['openAccBanktype'] : '',
			'openAccBankname' => isset($params['openAccBankname']) ? $params['openAccBankname'] : '',
			'openAccCityCode' => isset($params['openAccCityCode']) ? $params['openAccCityCode'] : ''
		];

		$this->check_require($data, [
			'requestNo',
			'merchantNo',
			'accountCardType',
			'accountNo',
			'accountName',
			'openAccBanktype'
		]);

		$myParams['bizReqJson'] = \GuzzleHttp\json_encode($data, JSON_UNESCAPED_UNICODE);
		return $this->app->basic->httpPost($this->api_urls['merchant'], $myParams, 'bizResponseJson');
	}

	/**
	 * @title 商户提现卡列表查询
	 *
	 * @param [type] $params
	 * @return void
	 */
	public function bankAccountQuery($params){
		$myParams = ['serviceNo'  => 'bankAccountQuery'];

		$data = [
			'requestNo'  => isset($params['requestNo']) ? $params['requestNo'] : '',
			'merchantNo' => isset($params['merchantNo']) ? $params['merchantNo'] : ''
		];

		$this->check_require($data, [
			'requestNo',
			'merchantNo'
		]);

		$myParams['bizReqJson'] = \GuzzleHttp\json_encode($data, JSON_UNESCAPED_UNICODE);
		return $this->app->basic->httpPost($this->api_urls['merchant'], $myParams, 'bizResponseJson');
	}

	/**
	 * @title 商户提现申请
	 *
	 * @param [type] $params
	 * @return void
	 */
	public function merchantWithdraw($params){
		$myParams = ['serviceNo'  => 'merchantWithdraw'];

		$data = [
			'requestNo'  => isset($params['requestNo']) ? $params['requestNo'] : '',   //业务请求号
			'merchantNo' => isset($params['merchantNo']) ? $params['merchantNo'] : '',  //提现商户银盛商户号
			'amount' => isset($params['amount']) ? $params['amount'] : '',  //提现金额，单位：元
			'accountType' => isset($params['accountType']) ? $params['accountType'] : '',  //提现资金账户类型 01-一般消费类账户（bankAccountNo必填） 02-待结算账户（商户结算方式为平台内时bankAccountNo必填，否则无法到账）
			'remark' => isset($params['remark']) ? $params['remark'] : '',  //银行附言，35个字节内
			'bankAccountNo' => isset($params['bankAccountNo']) ? $params['bankAccountNo'] : '',  //提现银行卡号（仅已绑定的提现卡或结算卡）
			'notifyUrl' => isset($params['notifyUrl']) ? $params['notifyUrl'] : ''  //提现结果异步通知地址，为空则不通知
		];

		$this->check_require($data, [
			'requestNo',
			'merchantNo',
			'amount',
			'accountType',
			'bankAccountNo'
		]);

		$myParams['bizReqJson'] = \GuzzleHttp\json_encode($data, JSON_UNESCAPED_UNICODE);
		return $this->app->basic->httpPost($this->api_urls['merchant'], $myParams, 'bizResponseJson');
	}

	/**
	 * @title 商户提现结果查询
	 *
	 * @param [type] $params
	 * @return void
	 */
	public function queryWithdraw($params){
		$myParams = ['serviceNo'  => 'merchantWithdrawQuery'];

		$data = [
			'requestNo'  => isset($params['requestNo']) ? $params['requestNo'] : ''
		];

		$this->check_require($data, [
			'requestNo'
		]);

		$myParams['bizReqJson'] = \GuzzleHttp\json_encode($data, JSON_UNESCAPED_UNICODE);
		return $this->app->basic->httpPost($this->api_urls['merchant'], $myParams, 'bizResponseJson');
	}

	/**
	 * @title 个人钱包注册
	 *
	 * @param [type] $params
	 * @return void
	 */
	public function walletRegister($params){
		$myParams = ['serviceNo'  => 'walletRegister'];

		$data = [
			'requestNo'  => isset($params['requestNo']) ? $params['requestNo'] : '',
			'name' => isset($params['name']) ? $params['name'] : '',
			'certNo' => isset($params['certNo']) ? $params['certNo'] : '',
			'certType' => isset($params['certType']) ? $params['certType'] : '',
			'mobile' => isset($params['mobile']) ? $params['mobile'] : '',
			'bankAccountNo' => isset($params['bankAccountNo']) ? $params['bankAccountNo'] : ''
		];

		$this->check_require($data, [
			'requestNo',
			'name',
			'certNo',
			'certType',
			'mobile',
		]);
		$myParams['bizReqJson'] = \GuzzleHttp\json_encode($data, JSON_UNESCAPED_UNICODE);
		return $this->app->basic->httpPost($this->api_urls['merchant'], $myParams, 'bizResponseJson');
	}

	/**
	 * @title 钱包信息查询
	 *
	 * @param [type] $params
	 * @return void
	 */
	public function userInfo($params){
		$myParams = ['serviceNo'  => 'userInfo'];

		$data = [
			'merchantNo' => isset($params['merchantNo']) ? $params['merchantNo'] : '',
			'certNo' => isset($params['certNo']) ? $params['certNo'] : '',
			'requestNo' => isset($params['requestNo']) ? $params['requestNo'] : '',
		];

		$myParams['bizReqJson'] = \GuzzleHttp\json_encode($data, JSON_UNESCAPED_UNICODE);
		return $this->app->basic->httpPost($this->api_urls['merchant'], $myParams, 'bizResponseJson');
	}

	/**
	 * @title 钱包绑卡（发送绑卡手机验证码）
	 *
	 * @param [type] $params
	 * @return void
	 */
	public function bindCard($params){
		$myParams = ['serviceNo'  => 'bindCard'];

		$data = [
			'requestNo' => isset($params['requestNo']) ? $params['requestNo'] : '',
			'merchantNo' => isset($params['merchantNo']) ? $params['merchantNo'] : '',
			'bindFunction' => isset($params['bindFunction']) ? $params['bindFunction'] : '',
			'bankAccountNo' => isset($params['bankAccountNo']) ? $params['bankAccountNo'] : '',
			'bankMobile' => isset($params['bankMobile']) ? $params['bankMobile'] : ''
		];

		$this->check_require($data, [
			'requestNo',
			'merchantNo',
			'bindFunction',
			'bankAccountNo',
			'bankMobile'
		]);
		$myParams['bizReqJson'] = \GuzzleHttp\json_encode($data, JSON_UNESCAPED_UNICODE);
		return $this->app->basic->httpPost($this->api_urls['merchant'], $myParams, 'bizResponseJson');
	}

	/**
	 * @title 钱包解绑银行卡
	 *
	 * @param [type] $params
	 * @return void
	 */
	public function unbind($params){
		$myParams = ['serviceNo'  => 'unbind'];

		$data = [
			'requestNo' => isset($params['requestNo']) ? $params['requestNo'] : '',
			'merchantNo' => isset($params['merchantNo']) ? $params['merchantNo'] : '',
			'linkId' => isset($params['linkId']) ? $params['linkId'] : ''  //绑卡标识
		];

		$this->check_require($data, [
			'requestNo',
			'merchantNo',
			'linkId'
		]);

		$myParams['bizReqJson'] = \GuzzleHttp\json_encode($data, JSON_UNESCAPED_UNICODE);
		return $this->app->basic->httpPost($this->api_urls['merchant'], $myParams, 'bizResponseJson');
	}

	/**
	 * 绑卡身份证补充上传
	 * @param $params
	 * @return mixed
	 */
	public function supplyCardPic($params){
		$myParams = ['serviceNo'  => 'supplyCardPic'];

		$data = [
			'requestNo' => isset($params['requestNo']) ? $params['requestNo'] : '',
			'merchantNo' => isset($params['merchantNo']) ? $params['merchantNo'] : '',
			'frontPicCode' => isset($params['frontPicCode']) ? $params['frontPicCode'] : '',  //身份证正面（人像）图片编码,统一文件上传接口返回的fileCode
			'backPicCode' => isset($params['backPicCode']) ? $params['backPicCode'] : '',  //身份证反面（国徽）图片编码
		];

		$this->check_require($params, ['requestNo', 'merchantNo', 'frontPicCode', 'backPicCode']);

		$myParams['bizReqJson'] = \GuzzleHttp\json_encode($data, JSON_UNESCAPED_UNICODE);
		return $this->app->basic->httpPost($this->api_urls['merchant'], $myParams, 'bizResponseJson');
	}

	/**
	 * @title 银行卡信息查询
	 *
	 * @param [type] $params
	 * @return void
	 */
	public function queryCard($params){
		$myParams = ['serviceNo'  => 'queryCard'];

		$data = [
			'bankAccountNo' => isset($params['bankAccountNo']) ? $params['bankAccountNo'] : ''
		];

		$this->check_require($data, [
			'bankAccountNo'
		]);
		
		$myParams['bizReqJson'] = \GuzzleHttp\json_encode($data, JSON_UNESCAPED_UNICODE);
		return $this->app->basic->httpPost($this->api_urls['merchant'], $myParams, 'bizResponseJson');
	}

	/**
	 * @title 个人钱包充值
	 *
	 * @param [type] $params
	 * @return void
	 */
	public function recharge($params){
		$myParams = ['serviceNo'  => 'recharge'];
		
		$data = [
			'requestNo' => isset($params['requestNo']) ? $params['requestNo'] : '',
			'merchantNo' => isset($params['merchantNo']) ? $params['merchantNo'] : '',
			'amount' => isset($params['amount']) ? $params['amount'] : '',
			'linkId' => isset($params['linkId']) ? $params['linkId'] : '',   //绑卡标识，提现银行卡对应的绑卡标识
			'remark' => isset($params['remark']) ? $params['remark'] : '',   //银行附言
			'notifyUrl' => isset($params['notifyUrl']) ? $params['notifyUrl'] : '' //提现结果异步通知地址，为空则不通知
		];

		$this->check_require($data, [
			'requestNo',
			'merchantNo',
			'amount',
			'linkId',
			'remark'
		]);

		$myParams['bizReqJson'] = \GuzzleHttp\json_encode($data, JSON_UNESCAPED_UNICODE);
		return $this->app->basic->httpPost($this->api_urls['merchant'], $myParams, 'bizResponseJson');
	}

	/**
	 * @title 个人钱包提现
	 *
	 * @param [type] $params
	 * @return void
	 */
	public function withdraw($params){
		$myParams = ['serviceNo'  => 'withdraw'];

		$data = [
			'requestNo' => isset($params['requestNo']) ? $params['requestNo'] : '',
			'merchantNo' => isset($params['merchantNo']) ? $params['merchantNo'] : '',
			'amount' => isset($params['amount']) ? $params['amount'] : '',
			'linkId' => isset($params['linkId']) ? $params['linkId'] : '',   //绑卡标识，提现银行卡对应的绑卡标识
			'remark' => isset($params['remark']) ? $params['remark'] : '',   //银行附言
			'notifyUrl' => isset($params['notifyUrl']) ? $params['notifyUrl'] : '' //提现结果异步通知地址，为空则不通知
		];

		$this->check_require($data, [
			'requestNo',
			'merchantNo',
			'amount',
			'linkId',
			'remark'
		]);

		$myParams['bizReqJson'] = \GuzzleHttp\json_encode($data, JSON_UNESCAPED_UNICODE);
		return $this->app->basic->httpPost($this->api_urls['merchant'], $myParams, 'bizResponseJson');
	}
}