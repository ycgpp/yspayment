<?php
// +----------------------------------------------------------------------
// | SentCMS [ WE CAN DO IT JUST THINK IT ]
// +----------------------------------------------------------------------
// | Copyright (c) 2013 http://www.tensent.cn All rights reserved.
// +----------------------------------------------------------------------
// | Author: molong <molong@tensent.cn> <http://www.tensent.cn>
// +----------------------------------------------------------------------
namespace tensent\yspay\merchant;

use Pimple\Container;
use Pimple\ServiceProviderInterface;

class MerchantProvider implements ServiceProviderInterface{
    
    public function register(Container $pimple) {
        $pimple['merchant'] = function ($app){
            return new Client($app);
        };
    }
}