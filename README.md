# yspayment

#### 介绍
银盛E企通支付接口

#### 软件架构

* PHP


#### 安装教程

~~~
composer require tensent/yspay
~~~

#### 使用说明

~~~php
use tensent\yspay\Client;

$config = [
	'business_gate_cert'    => '',    //公钥路径
	'private_cert'          => './private_key.pem',           //私钥路径
	'partner_id'            => '826************061', //商户号
	'seller_id'             => '',
	'seller_name'           => '',

	'pfxpassword'           => '',
	'merchant_code'         => '',

	'business_code'         => '',
	'log'   => [
		'path' => __DIR__ . '/info.log',
		'name'  => 'ysepay',
	],
	'sign_type'       => 'RSA',
	'appid'            => '公众号appid',
	'weapp_appid' => '小程序appid',
]

$app = new Client($config);

// 钱包注册
$regData = [
    'requestNo' => uniqid(),
    'name' => $data['cname'],
    'certNo' => $data['certNo'],
    'certType' => '01',
    'mobile' => $data['mobile']
];
$result = $app->merchant->walletRegister($regData);

//js聚合支付

$data = [
	'payeeMerchantNo' => config('payment.partner_id'), // 收款方银盛商户号
	'order_number' => $order['orderno'], // 商户系统生成的订单号
	'amount' => $order['price'], // 交易金额 number_format($price, 2, '.', '')
	'orderDesc' => $order['goods_name'], // 商品名称/订单标题 
	'bankType' => '1902000', // 支付渠道 支付渠道1902000-微信(支持payMode：28、29) 1903000-支付宝(支持payMode：26)9001002-银联(支持payMode：30)
	'payMode' => '28', //支付方式26-支付宝生活号 、28-微信公众号 29-微信小程序、30-银联行业码支付
	'wxAppId' => config('payment.appid'),
	'wxOpenId' => $order['openid'],
	'notifyUrl' => request()->domain() . '/api/pay/notify/order' // 支付成功结果异步通知地址
];
$payInfo = $client->trade->jspay($data);
~~~

此项目仅为方便服务商对接银盛E企通支付接口，涉及的支付如下：
1. 商户类接口
2. 交易类接口

银盛E企通支付接口文档：https://www.yuque.com/bgu2ty/eqt/ugno56

#### 鸣谢

1.  [tu6ge](https://github.com/tu6ge/ysepay-php-sdk)

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request
